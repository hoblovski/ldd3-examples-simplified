#include <linux/fs.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <uapi/linux/fs.h>

#define SCULL_NAME "scull"
#define SCULL_DATASIZE 4096
#define SCULL_NR_DEVS 4


struct scull_dev {
	void* data;			/* data */
	struct mutex dev_lock;		/* io lock */
	struct cdev cdev;		/* char dev */
};


static dev_t scull_major = 0;
static dev_t scull_baseminor = 0;
static struct scull_dev scull_devs[SCULL_NR_DEVS];


// llseek must be implemented
// otherwise reasonable read will not terminate (never seeing EOF)
static loff_t scull_llseek(struct file *filp, loff_t off, int whence)
{
	loff_t newpos;

	switch (whence) {
		case SEEK_CUR:
			newpos = filp->f_pos + off;
			break;
		case SEEK_SET:
			newpos = off;
			break;
		case SEEK_END:
			newpos = SCULL_DATASIZE + off;
			break;
		default:
			return -EINVAL;
	}
	if (newpos < 0 || newpos > SCULL_DATASIZE)
		return -EINVAL;
	filp->f_pos = newpos;
	return newpos;
}


static int scull_open(struct inode *inode, struct file *filp)
{
	struct scull_dev *dev =
		container_of(inode->i_cdev, struct scull_dev, cdev);
	filp->private_data = dev;

	return 0;
}


int scull_release(struct inode *inode, struct file *filp)
{
	return 0;
}


static ssize_t scull_read(struct file *filp, char __user *buf,
		size_t count, loff_t *f_pos)
{
	struct scull_dev *dev = filp->private_data;
	int err;

	printk(KERN_INFO "scull: read %lu\n", count);

	if (*f_pos > SCULL_DATASIZE) {
		return 0;
	}
	if (*f_pos + count > SCULL_DATASIZE) {
		count = SCULL_DATASIZE - *f_pos;
	}
	if (mutex_lock_interruptible(&(dev->dev_lock))) {
		return -ERESTARTSYS;
	}
	if (copy_to_user(buf, dev->data + *f_pos, count)) {
		err = -EFAULT;
		goto fail;
	}

	*f_pos += count;	
	mutex_unlock(&(dev->dev_lock));
	return count;

fail:
	mutex_unlock(&(dev->dev_lock));
	return err;
}


static ssize_t scull_write(struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	int err;
	struct scull_dev *dev = filp->private_data;

	printk(KERN_INFO "scull: write %lu\n", count);
	if (count > SCULL_DATASIZE) {
		return -EINVAL;
	}
	if (mutex_lock_interruptible(&(dev->dev_lock))) {
		return -ERESTARTSYS;
	}
	if (copy_from_user(dev->data, buf, count)) {
		err = -EFAULT;
		goto fail;
	}
	mutex_unlock(&(dev->dev_lock));
	return count;

fail:
	mutex_unlock(&(dev->dev_lock));
	return err;
}


static struct file_operations scull_fops = {
	.owner = THIS_MODULE,
	.llseek = scull_llseek,
	.read = scull_read,
	.write = scull_write,
	.open = scull_open,
	.release = scull_release,
};


static void scull_cleanup_module(void)
{
	int i;
	dev_t devno = MKDEV(scull_major, scull_baseminor);
	unregister_chrdev_region(devno, SCULL_NR_DEVS);

        /* Release memory */
	for (i = 0; i < SCULL_NR_DEVS; i++) {
		struct scull_dev *dev = &(scull_devs[i]);
		kfree(dev->data);
	}

	printk(KERN_INFO "scull bye\n");
}


static int scull_init_module(void)
{
	int err;
	int i;
	dev_t devno;

	printk(KERN_INFO "scull init begin");

	/* Determine major number */
	err = alloc_chrdev_region(&devno, scull_baseminor, SCULL_NR_DEVS,
			SCULL_NAME);
	if (err < 0) {
		printk(KERN_WARNING "scull: can't obtain major\n");
		goto fail;
	}
	scull_major = MAJOR(devno);

        /* Initialize devices */
	for (i = 0; i < SCULL_NR_DEVS; i++) {
		struct scull_dev *dev = &(scull_devs[i]);
		dev->data = kmalloc(SCULL_DATASIZE, GFP_KERNEL);
		if (dev->data == NULL) {
			err = -ENOMEM;
			goto fail;
		}
		memset(dev->data, 'x', SCULL_DATASIZE);
		mutex_init(&(dev->dev_lock));
		devno = MKDEV(scull_major, scull_baseminor + i);
		cdev_init(&(dev->cdev), &scull_fops);
		dev->cdev.owner = THIS_MODULE;
		dev->cdev.ops = &scull_fops;
		err = cdev_add(&(dev->cdev), devno, 1);
		if (err)
			printk(KERN_WARNING "scull: scull%d add cdev failed with %d\n",
					i, err);
	}

	printk(KERN_INFO "scull init success! major=%d, baseminor=%d\n", scull_major, scull_baseminor);
	return 0;

fail:
	printk(KERN_ERR "scull init failed.\n");
	scull_cleanup_module();
	return err;
}


module_init(scull_init_module);
module_exit(scull_cleanup_module);
MODULE_AUTHOR("Hoblovski");
MODULE_LICENSE("GPL");
